#ifndef ADRESSE_H
#define ADRESSE_H

#include <iostream>
using namespace std;

class Adresse
{
    public:
        Adresse(string="VIDE", string=" ", int=00000, string="VIDE");
        virtual ~Adresse();

        string GetLib() { return Lib; }
        void SetLib(string val) { Lib = val; }
        string GetComp() { return Comp; }
        void SetComp(string val) { Comp = val; }
        string GetVille() { return Ville; }
        void SetVille(string val) { Ville = val; }
        int GetCP() { return CP; }
        void SetCP(int val) { CP = val; }

        void affiche();

    protected:

    private:
        string Lib;
        string Comp;
        string Ville;
        int CP;
};

#endif // ADRESSE_H
