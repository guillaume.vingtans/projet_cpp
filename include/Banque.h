#ifndef BANQUE_H
#define BANQUE_H
#include <iostream>
#include<vector>
#include <algorithm>
using namespace std;
#include"Client.h"

class Banque
{
    public:
        Banque(string="NONAME");
        virtual ~Banque();

        void ajouter(Client *);
        void supprimer(int=0);
        void afficherPers(int=0);

        void getClients();
        int rechByNom(string);

        void afficheCB(int);

        void modifier(int);

    protected:

    private:
        vector<Client*> vecClient;
        string nom;

};

#endif // BANQUE_H
