#ifndef CLIENT_H
#define CLIENT_H
#include<vector>
#include <algorithm>
#include <iostream>
#include <string>
using namespace std;

#include "Adresse.h"
#include "CompteB.h"

class Client
{
    public:
         vector <CompteB*> VecCB;


        Client(string="VIDE",string="VIDE",char='F',string="0000000000", string="VIDE", string=" ", int=00000, string="VIDE");
        virtual ~Client();

        string Getnom() { return nom; }
        void Setnom(string);
        string Getprenom() { return prenom; }
        void Setprenom(string);
        char Getsexe() { return sexe; }
        void Setsexe(char);
        string Gettel() { return tel; }
        void Settel(string);

        void setAdresse(string,string,int,string);

        virtual void affiche();
        virtual void afficheCB()=0;


        virtual void modifier(int)=0;

        void creaCB(long int ,int ,int ,int,double ,float);

    protected:

    private:
        string nom;
        string prenom;
        char sexe;
        string tel;
        Adresse * adresseP=new Adresse;
};

#endif // CLIENT_H
