#ifndef COMPTEB_H
#define COMPTEB_H
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
using namespace std;

class CompteB
{
    public:
        CompteB(long int=00000,int=0,int=0,int=0,double=0.0,float=0.0);
        virtual ~CompteB();

        long int GetNumCB() { return NumCB; }
        void SetNumCB(long int val) { NumCB = val; }
        double GetSolde() { return Solde; }
        void SetSolde(double val) { Solde = val; }
        float GetDecouvertOK() { return DecouvertOK; }
        void SetDecouvertOK(float val) { DecouvertOK = val; }

        void setDateN(int, int, int);
        string getDateN();

        void operator += (double);

        void afficheCB();

    protected:

    private:
        long int NumCB;
        double Solde;
        float DecouvertOK;
        vector <int>DateOuv;
};

#endif // COMPTEB_H
