#ifndef PARTICULIER_H
#define PARTICULIER_H
#include <iostream>
#include <sstream>
#include <vector>

using namespace std;
#include "Client.h"

enum class SituationFamiliale {Celibataire, Marie, Divorce, Autre};

class Particulier : public Client
{
    public:
        Particulier();
        Particulier(string, string,char,string, string,string, int, string,SituationFamiliale,int ,int,int);
        Particulier(string, string);

        virtual ~Particulier();

        string getSitu();
        void setSitu(SituationFamiliale);

        void setDateN(int,int,int);
        string getDateN();

        void affiche();
        void afficheCB();

        void modifier(int);

    protected:

    private:
        SituationFamiliale situFam;
        vector <int>DateNaiss;
};

#endif // PARTICULIER_H
