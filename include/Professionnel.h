#ifndef PROFESSIONNEL_H
#define PROFESSIONNEL_H

#include "Client.h"

class Professionnel : public Client
{
    public:
        Professionnel();
        Professionnel(string, string,char,string, string,string, int, string,string,string, int, string, string,int, string,string);
        Professionnel(string, string);
        virtual ~Professionnel();

        string Getsiret() { return siret; }
        void Setsiret(string);
        string GetRSociale() { return RSociale; }
        void SetRSociale(string);
        int GetanneeCrea() { return anneeCrea; }
        void SetanneeCrea(int);
        string Getmail() { return mail; }
        void Setmail(string);

        void SetadresseE(string,string,int,string);

        void affiche();
        void afficheCB();

        void modifier(int);

    protected:

    private:
        string siret;
        string RSociale;
        int anneeCrea;
        Adresse * adresseE=new Adresse;
        string mail;
};

#endif // PROFESSIONNEL_H
