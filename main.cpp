// PROGRAMME PAR GUILLAUME VINGTANS //
//                      23/06/2020                        //

#include <iostream>
#include <ctype.h>
#include <windows.h>
using namespace std;
#include "Client.h" // a supprimer apr�s test
#include "Particulier.h"
#include "Professionnel.h"
#include "Banque.h"

void ASM_Client(Banque*);

int main()
{

/*
    Banque*  B1 = new Banque;

//    Client c1;
//    c1.affiche();
    cout<<"---------------------------------------------------------"<<endl;
   Client c2("Trump","Donald",'M',"0123456789","Maison Blanche","A202",88888,"Washington");
//    c2.affiche();
    cout<<"---------------------------------------------------------"<<endl;
    Client c3("Poutine","Vladimir",'M',"9876543210","Kremlin","",99999,"Moscou");
 //   c3.affiche();
    cout<<"---------------------------------------------------------"<<endl;
   Client c4("Merkel","Angela",'F',"0984512368","Bundeskanzleramt","",123456,"Berlin");
//    c4.affiche();
    cout<<"---------------------------------------------------------"<<endl;
    Particulier * p1 = new Particulier("Rin", "Tintin", 'M',"0984512378","Rue des oiseaux","Bis",33000,"Bordeaux",SituationFamiliale::Marie,10,9,1985);
 //   p1.affiche();
    cout<<"---------------------------------------------------------"<<endl;
    Professionnel pro1;
  //  pro1.affiche();
    cout<<"---------------------------------------------------------"<<endl;


    Professionnel * pro2 = new Professionnel("LeBricoleur","Bob",'M',"9988776655","2 Rue de la Pelleteuse", "TER",75000,"Paris","01234567890123","9999999999",2012,"3 Avenue des champs elysees","",75000,"Paris","Bob.lebricolo@BTP.com");


    B1->ajouter(pro2);
    B1->ajouter(p1);
    B1->afficherPers(1);
    B1->getClients();
    B1->supprimer(0);



    Professionnel * pro2 = new Professionnel("Bricolo","Bob",'M',"9988776655","2 Rue de la Pelleteuse", "TER",75000,"Paris","01234567890123","9999999999",2012,"3 Avenue des champs elysees","",75000,"Paris","Bob.lebricolo@BTP.com"); //A supp
    Professionnel * pro3 = new Professionnel("Bolo","Bob",'M',"9988776655","2 Rue de la Pelleteuse", "TER",75000,"Paris","01234567890123","9999999999",2012,"3 Avenue des champs elysees","",75000,"Paris","Bob.lebricolo@BTP.com"); //A supp
    Particulier * p1 = new Particulier("Rin", "Tintin", 'M',"0984512378","Rue des oiseaux","Bis",33000,"Bordeaux",SituationFamiliale::Marie,10,9,1985);

    p1->creaCB(10,2,10,2020,1000.3,50.50);
    pro2->creaCB(20,5,12,2019,100000,30.1);
    pro3->creaCB(30,12,12,2018,-50.3,0);

    p1->afficheCB();
    pro3->afficheCB();

//LIGNES SUPERIEURES DU MAIN POUR TEST RAPIDE
*/

    string bankN("NONAME");
    char choix=' ';
    string NomC(" ");
    int pos=-1;
    int NumbC=0;
    double val;
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hConsole, 6); // Pourquoi pas ajouter un r�glage de la couleur au logiciel ?

    cout << "******************** APPLICATION v1 ********************"<<endl;
    cout <<"************** GESTION DE COMPTES CLIENTS **************"<<endl;

    cout<<endl<<"Quel est le nom de la Banque ?"<<endl;
    cin>>bankN;
    Banque*  BANK = new Banque(bankN);

 //   BANK->ajouter(pro2); //A supp
 //   BANK->ajouter(pro3); //A supp
 //   BANK->ajouter(p1); //A supp

    cout <<endl <<"=====================MENU PRINCIPAL====================="<<endl;


etiqu1:
    cout <<endl<< "Choix ? "<<endl;
    cout << "   L : Lister l'ensemble des clients"<<endl;
    cout << "   S : Consulter les soldes des comptes pour un client"<<endl;
    cout << "   C : Ajouter/Supprimer/Modifier un client"<<endl;
    cout << "   O : Ajouter/Supprimer/Modifier une operation (WIP ...) "<<endl;
    cout << "   A : Afficher l'ensemble des operations pour un compte(WIP ...) "<<endl;
    cout << "   I  : Importer le fichier des operations Bancaires(WIP ...) "<<endl;
    cout << "   Q : Quitter .. "<<endl;
    cin >> choix;
    choix=toupper(choix);

try{

    switch(choix) {
    case 'L':
        cout << "Liste de l'ensemble des clients" << endl;

        BANK->getClients();

        goto etiqu1;
        break;

    case 'S':
        cout << "Consulter les soldes des comptes pour un client"<< endl;
        cout <<endl<< "Merci de saisir le Nom du client : "<<endl;
        cin >> NomC;
        pos=BANK->rechByNom(NomC);
        if (pos!=-1)
        {BANK->afficheCB(pos);}
        goto etiqu1;
        break;

    case 'C':
        cout << "Ajouter/Supprimer/Modifier un client"<< endl;

        ASM_Client(BANK);

        goto etiqu1;
        break;

    case 'O':
        cout << "Ajouter/Supprimer/Modifier une operation"<< endl;
/*
        cout <<endl<< "Merci de saisir le Nom du client : "<<endl;
        cin >> NomC;
        pos=BANK->rechByNom(NomC);
        if (pos!=-1)
        {
            cout <<endl<< "Merci de saisir le num de compte : "<<endl;
            cin >> NumbC;
            cout <<endl<< "Merci de saisir la valeur de l'op�ration : "<<endl;
            cin >> val;
            (BANK->RenvClient(NumbC))->VecCB[NomC])+=val;
        }
*/
        goto etiqu1;
        break;

    case 'A':
        cout << "Affichage des operations pour un compte"<< endl;

        //fonction();

        goto etiqu1;
        break;

    case 'I':
        cout << "Importer le fichier des operations Bancaires"<< endl;

        //fonction();

        goto etiqu1;
        break;

    case 'Q':
        cout <<endl<<"Merci d'avoir utilise le programme"<<endl<<"Fermeture du programme ..."<<endl;
        break;

    default:
         cout <<endl<<"** ERREUR - Resaisir le choix ** "<<endl;
        goto etiqu1;
        break;
    }

}
catch (...)
{
    cerr<<"*** ERREUR ***"<<endl;
}
    cout<<endl;
    delete BANK;
    return 0;
}

void ASM_Client(Banque* BANK)
{
    int pos=0;
    char ASM_choix=' ';
    string NomC("VIDE");
    string PrenomC("VIDE");
    char TypeC=' ';

    cout <<endl<< "Choix ? "<<endl;
    cout << "   A : Ajouter client"<<endl;
    cout << "   S : Supprimer client"<<endl;
    cout << "   M : Modifier client"<<endl;
    cin >> ASM_choix;
    ASM_choix=toupper(ASM_choix);

    switch(ASM_choix) {
        case 'A':
            cout << " -- Ajouter rapide client --  "<<endl;
            cout << "Merci de saisir le Nom du nouveau client : "<<endl;
            cin >> NomC;
            cout << "Merci de saisir le Prenom du nouveau client : "<<endl;
            cin >> PrenomC;
            cout << "Pro (P) ou Particulier (A) ? "<<endl;
            cin >> TypeC; TypeC=toupper(TypeC);
            if (TypeC=='P')
            {
               Professionnel * Npro = new Professionnel(NomC,PrenomC);
               BANK->ajouter(Npro);
               cout<< "FAIT !"<<endl;
            }
            else
            {
                if (TypeC=='A')
                {
                   Particulier * Npar= new Particulier(NomC,PrenomC);
                   BANK->ajouter(Npar);
                   cout<< "FAIT !"<<endl;
                }
                else
                    {
                        cout<< "ERREUR DANS LE CHOIX"<<endl;
                        cout<< "Retour au Menu Principal...."<<endl;
                    }
            }
            break;

            case 'S':
                cout << " -- Suppression rapide client --  "<<endl;
                cout << "Merci de saisir le Nom du client : "<<endl;
                cin >> NomC;
                pos=BANK->rechByNom(NomC);
                if (pos!=-1)
                {BANK->supprimer(pos);}
                break;

            case 'M':
                cout << " -- Modification client --  "<<endl;
                cout << "Merci de saisir le Nom du client : "<<endl;
                cin >> NomC;
                cout<<"Selectionner le champ a modifier : "<<endl;
                cout << "   1 : Le nom du client"<<endl;
                cout << "   2 : Le prenom du client"<<endl;
                cout << "   3 : Le sexe du client"<<endl;
                cout << "   4 : Le tel du client"<<endl;
                cout << "   5 : L'adresse du client"<<endl;

                pos=BANK->rechByNom(NomC); //position dans vecClient
                BANK->modifier(pos);
                cout<< "FAIT !"<<endl;
                break;
    }
}
