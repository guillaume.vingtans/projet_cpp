#include "Adresse.h"

Adresse::Adresse(string Lib, string Comp, int CP, string Ville)
{
    SetLib(Lib);
    SetComp(Comp);
    SetCP(CP);
    SetVille(Ville);
}

Adresse::~Adresse()
{
    cout<< "Destruction Adresse : "<<GetLib()<<" "<<GetVille()<<endl;
}


void Adresse::affiche()
{
    cout << "Adresse : "<<endl<< GetLib() << " " << GetComp()<<endl;
    cout << GetCP()<< " - " <<GetVille()<<endl;
}
