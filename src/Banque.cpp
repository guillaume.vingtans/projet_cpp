#include "Banque.h"

Banque::Banque(string nom)
{
    this->nom=nom;
}

Banque::~Banque()
{
    for(int i=0; i < vecClient.size() ; i++)
    {
        delete vecClient[i];
    }
    cout << "Destruction de la Banque " << nom << endl;
}

void Banque::ajouter(Client* p)
{
    vecClient.push_back(p);
}

void Banque::supprimer(int posASupprimer)
{
    if (posASupprimer < vecClient.size())
    {
        vecClient.erase(vecClient.begin() + posASupprimer);
        cout<<"Suppression du Client : OK"<<endl;
    }
    else
        cout << "Aucun element ne correspond a cette position : " << posASupprimer << endl;
}

void Banque::afficherPers(int posARechercher)
{
    auto p= vecClient.at(posARechercher);
    p->affiche();
}

void Banque::getClients()
{
    if (vecClient.size()!=0)
    {
        cout<<endl<< "====== Liste des clients dans la banque ====== "<<endl<<vecClient.size()<<" client(s) a afficher ..."<<endl<<endl;
        for(int i=0; i < vecClient.size() ; i++)
        {
            cout<<"Numero Client : "<<i<<endl;
            afficherPers(i);
            cout<<"---------------------------------------------------------"<<endl;
        }
    }
    else{cout<<endl<<"** Pas de clients dans la banque ... **"<<endl;}
}

int Banque::rechByNom(string nomARech)
{
    int index=-1;
    vector<string> VecNom;
        for(int i=0; i < vecClient.size() ; i++)
        {
                VecNom.push_back(vecClient[i]->Getnom());
        }
        vector<string>::iterator it = find(VecNom.begin(),VecNom.end(), nomARech);
        if(it != VecNom.end())
        {
            index=distance(VecNom.begin(), it);
            return index;
        }
        else
        {
            cerr<<"Sherlock n'a pas retrouve votre client ..."<<endl;
        }
}

void Banque::modifier(int pos)
{
    vecClient.at(pos)->modifier(pos);
}


void Banque::afficheCB(int posARechercher)
{
        auto p= vecClient.at(posARechercher);
        p->afficheCB();
}

