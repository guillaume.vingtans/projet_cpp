#include "CompteB.h"

CompteB::CompteB(long int num,int j,int m,int a,double s,float dec)
{
    SetNumCB(num);
    SetSolde(s);
    SetDecouvertOK(dec);
    setDateN(j, m, a);
}

CompteB::~CompteB()
{
    cout << "Destruction du compte n� " << GetNumCB() << endl;
}

void CompteB::setDateN(int j, int m, int a)
{
    if ((j<32)&&(j>0)) {
        DateOuv.push_back(j);
    } else {
        cout<<"** JOUR INVALIDE (-> 1 par defaut) **"<<endl;
        DateOuv.push_back(1);
    }

    if ((m<13)&&(m>0)) {
        DateOuv.push_back(m);
    } else {
        cout<<"** MOIS INVALIDE (-> 1 par defaut) **"<<endl;
        DateOuv.push_back(1);
    }
    if ((a<3000)&&(a>0)) {
        DateOuv.push_back(a);

    } else {
        cout<<"** ANNEE INVALIDE (-> 1900 par defaut) **"<<endl;
        DateOuv.push_back(1900);
    }

}

string CompteB::getDateN()
{
    ostringstream oss;
    oss<<DateOuv.at(0)<<"/"<<DateOuv.at(1)<<"/"<<DateOuv.at(2);
    return oss.str();
}

 void CompteB::afficheCB()
 {
    cout<<"================================"<<endl;
    cout<<"Compte n"<<GetNumCB()<<endl;
    cout<<"---------------------------------------------------------"<<endl;
    cout<<"Ouvert le : "<<getDateN()<<endl;
    cout<<"---------------------------------------------------------"<<endl;
    cout<<"Decouvert autorise : "<<GetDecouvertOK()<<" E"<<endl;
    cout<<"---------------------------------------------------------"<<endl;
    cout<<"Solde actuel : "<<GetSolde()<<" E"<<endl;
    cout<<"================================"<<endl;
 }

 void CompteB::operator += (double add)
{
    SetSolde(GetSolde()+add);
}
