#include "Particulier.h"

Particulier::Particulier()
{
    Setnom("VIDE");
    Setprenom("VIDE");
    Setsexe('F');
    Settel("0000000000");
    setAdresse("VIDE", " ", 00000, "VIDE");
    setSitu(SituationFamiliale::Autre);
    setDateN(01,01,1900);
}

Particulier::Particulier(string n, string p,char sex,string t, string l,string c, int copo, string v,SituationFamiliale s,int j,int m,int a)
{
    Setnom(n);
    Setprenom(p);
    Setsexe(sex);
    Settel(t);
    setAdresse(l, c, copo, v);
    setSitu(s);
    setDateN(j,m,a);

}

Particulier::Particulier(string n, string p)
{
    Setnom(n);
    Setprenom(p);
    Setsexe('F');
    Settel("0000000000");
    setAdresse("VIDE", " ", 00000, "VIDE");
    setSitu(SituationFamiliale::Autre);
    setDateN(01,01,1900);
}


Particulier::~Particulier()
{
    cout << "Destruction Particulier :" <<Getnom()<<endl;
}


string Particulier::getSitu()
{
    string libelle="\0";
    switch(situFam)
    {
        case SituationFamiliale::Celibataire:
            libelle= "Celibataire";
            break;
        case SituationFamiliale::Marie:
            libelle= "Marie(e)";
            break;
        case SituationFamiliale::Divorce:
            libelle= "Divorce(e)";
            break;
        default:
            libelle= "Autre";
            break;
    }
    return libelle;
}

void Particulier::setSitu(SituationFamiliale situ)
{
    situFam = situ;
}

void Particulier::setDateN(int j, int m, int a)
{
    if ((j<32)&&(j>0)) {
        DateNaiss.push_back(j);
    } else {
        cout<<"** JOUR INVALIDE (-> 1 par defaut) **"<<endl;
        DateNaiss.push_back(1);
    }

    if ((m<13)&&(m>0)) {
        DateNaiss.push_back(m);
    } else {
        cout<<"** MOIS INVALIDE (-> 1 par defaut) **"<<endl;
        DateNaiss.push_back(1);
    }
    if ((a<3000)&&(a>0)) {
        DateNaiss.push_back(a);

    } else {
        cout<<"** ANNEE INVALIDE (-> 1900 par defaut) **"<<endl;
        DateNaiss.push_back(1900);
    }

}

string Particulier::getDateN()
{
    ostringstream oss;
    oss<<DateNaiss.at(0)<<"/"<<DateNaiss.at(1)<<"/"<<DateNaiss.at(2);
    return oss.str();
}

void Particulier::affiche()
{
    cout<<"** Informations client **"<<endl;
    Client::affiche();
    cout<<"Situation Familiale : "<<getSitu()<<endl;
    cout<<"Date de naissance : "<<getDateN()<<endl;
}

void Particulier::afficheCB()
{
   for(int i=0; i < VecCB.size() ; i++)
    {
        VecCB[i]->afficheCB();
    }
}


void Particulier::modifier(int x)
{
    char c=0;

    string s("");
    string s1("");
    string s2("");
    int j=0;
    int m=0;
    int a=0;
    char L = 'F';
    cout << "   6 : Situation familiale du client"<<endl;
    cout << "   7 : Date de naissance du client"<<endl;
    cin>>c;
    cout << "==> Quelle est la nouvelle valeur ";
    switch(c) {
        case '1':
            cout << "du nom : ";
            cin>>s;
            Setnom(s);
            break;
        case '2':
            cout << "du prenom : ";
            cin>>s;
            Setprenom(s);
            break;
        case '3':
            cout << "du sexe : ";
            cin>>L;
            Setsexe(L);
            break;
        case '4':
            cout << "du telephone : ";
            cin>>s;
            Settel(s);
            break;
        case '5':
            cout << "du libelle de l'adresse : ";
            cin>>s;
            cout <<endl<< "du complément de l'adresse : ";
            cin>>s1;
            cout <<endl<<  "du CP : ";
            cin>> a;
            cout <<endl<<  "de la ville : ";
            cin>>s2;
            setAdresse(s,s1,a,s2);
            break;
        case '6':
            cout << "de la situation familiale (1 : Celibataire / 2 : Marie(e) / 3 : Divorce(e) / 0 : Autre)  ";
            cin>>a;
            if (a==1){situFam=SituationFamiliale::Celibataire;}
            if (a==2){situFam=SituationFamiliale::Marie;}
            if (a==3){situFam=SituationFamiliale::Divorce;}
            if (a==0){situFam=SituationFamiliale::Autre;}
            break;
        case '7':
            cout << "du jour de la date de naissance (sans commencer par '0'): ";
            cin>>j;
            cout << "du mois de la date de naissance (sans commencer par '0'): ";
            cin>>m;
            cout << "de l'annee de la date de naissance (format '1900'): ";
            cin>>a;
            setDateN(j,m,a);
            break;
        default:
            cout<<endl<<"Erreur - Modification non prise en compte !"<<endl;
            break;

    }
}








