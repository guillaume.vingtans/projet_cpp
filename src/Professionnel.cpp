#include "Professionnel.h"

Professionnel::Professionnel()
{
     Setnom("VIDE");
    Setprenom("VIDE");
    Setsexe('F');
    Settel("0000000000");
    setAdresse("VIDE", " ", 00000, "VIDE");
    Setsiret("0000000000");
    SetRSociale("0000000000");
    SetanneeCrea(1900);
    SetadresseE("VIDE"," ",00000,"VIDE");
    Setmail("xxxxx.xxxxx@xxxxx.xx");
}

Professionnel::Professionnel(string n, string p,char sex,string t, string l,string c, int copo, string v, string si, string RS, int an, string l2, string c2, int copo2, string v2, string e)
{
    Setnom(n);
    Setprenom(p);
    Setsexe(sex);
    Settel(t);
    setAdresse(l, c, copo, v);
    Setsiret(si);
    SetRSociale(RS);
    SetanneeCrea(an);
    SetadresseE(l2,c2,copo2,v2);
    Setmail(e);
}

Professionnel::Professionnel(string n, string p)
{
    Setnom(n);
    Setprenom(p);
    Setsexe('F');
    Settel("0000000000");
    setAdresse("VIDE", " ", 00000, "VIDE");
    Setsiret("0000000000");
    SetRSociale("0000000000");
    SetanneeCrea(1900);
    SetadresseE("VIDE"," ",00000,"VIDE");
    Setmail("xxxxx.xxxxx@xxxxx.xx");
}



Professionnel::~Professionnel()
{
    delete adresseE;
    cout<< "Destruction Pro : "<<Getprenom()<<" "<<Getnom()<<endl;
}


void Professionnel::Setsiret(string s)
{
    if ((s.length()<15)&&(s.length()>0))
    {
        siret=s;
    }
    else
    {
        cout << "Siret Invalide"<<endl;
        siret="00000000000000";
    }

}

void Professionnel::SetRSociale(string RS)
{
    if ((RS.length()<51)&&(RS.length()>0))
    {
        RSociale=RS;
    }
    else
    {
        cout << "Mauvaise raison sociale"<<endl;
        RSociale="00000000000000";
    }
}

void Professionnel::SetanneeCrea(int a)
{
    if ((a<3000)&&(a>0))
    {
        anneeCrea=a;
    }
    else
        anneeCrea=1900;
}


void Professionnel::SetadresseE(string l, string c, int copo, string v)
{
    adresseE->SetLib(l);
    adresseE->SetComp(c);
    adresseE->SetCP(copo);
    adresseE->SetVille(v);
}


void Professionnel::Setmail(string email)
{
    bool ok=0;
    for (int i=0;i<email.length();++i)
    {
        char c = email[i];
        if (c=='@')
        {
            ok=1;
        }
    }
    if (ok)
    {
        mail=email;
    }
    else
    {
        cout << "Mauvais Email"<<endl;
        mail="xxxxx.xxxxx@xxxxx.xx";
    }

}

void Professionnel::affiche()
{
    cout<<"** Informations client **"<<endl;
    Client::affiche();
    cout<<"** Informations de l'entreprise **"<<endl;
    cout<<"Num Siret : "<<Getsiret()<<endl;
    cout<<"Raison Sociale : "<<GetRSociale()<<endl;
    cout<<"Annee Creation : "<<GetanneeCrea()<<endl;
    adresseE->affiche();
    cout<<"E-Mail : "<<Getmail()<<endl;
}

void Professionnel::afficheCB()
{
   for(int i=0; i < VecCB.size() ; i++)
    {
        VecCB[i]->afficheCB();
    }
}



void Professionnel::modifier(int x)
{
    char c=0;

    string s("");
    string s1("");
    string s2("");
    int a=0;
    char L = 'F';

    cout << "   6 : Siret du client"<<endl;
    cout << "   7 : Raison sociale du client"<<endl;
    cout << "   8 : Annee de creation de l'entreprise du client"<<endl;
    cout << "   9 : adresse de l'entreprise du client"<<endl;
    cout << "   0 : Email du client"<<endl;
    cin>>c;

    cout << "Quelle est la nouvelle valeur ";
    switch(c) {
        case '1':
            cout << "du nom : ";
            cin>>s;
            Setnom(s);
            break;
        case '2':
            cout << "du prenom : ";
            cin>>s;
            Setprenom(s);
            break;
        case '3':
            cout << "du sexe : ";
            cin>>L;
            Setsexe(L);
            break;
        case '4':
            cout << "du telephone : ";
            cin>>s;
            Settel(s);
            break;
        case '5':
            cout << "du libelle de l'adresse : ";
            cin>>s;
            cout <<endl<< "du complément de l'adresse : ";
            cin>>s1;
            cout <<endl<<  "du CP : ";
            cin>> a;
            cout <<endl<<  "de la ville : ";
            cin>>s2;
            setAdresse(s,s1,a,s2);
            break;
        case '6':
            cout << "du Siret : ";
            cin>>s;
            Setsiret(s);
            break;
        case'7':
            cout << "de la raison sociale : ";
            cin>>s;
            SetRSociale(s);
            break;
        case'8':
            cout << "de l'annee de creation : ";
            cin>>a;
            SetanneeCrea(a);
            break;
        case'9':
            cout << "du libelle de l'adresse de l'entreprise : ";
            cin>>s;
            cout <<endl<< "du complément de l'adresse de l'entreprise : ";
            cin>>s1;
            cout <<endl<<  "du CP de l'entreprise: ";
            cin>> a;
            cout <<endl<<  "de la ville de l'entreprise : ";
            cin>>s2;
            SetadresseE(s,s1,a,s2);
            break;
         case'0':
            cout << "du mail (avec @) : ";
            cin>>s;
            Setmail(s);
            break;
        default:
            cout<<endl<<"Erreur - Modification non prise en compte !"<<endl;
            break;
    }
}


